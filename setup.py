#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='befree',
      version='0.1',
      packages=find_packages(),
      install_requires=[
          "inflect",
          "nltk",
          "regex",
          "tqdm",
          "pymongo",
          "progressbar",
      ],
      # package_dir={'befree': 'befree'},
      package_data={"": ["*.pkl", "*.bin", "*.tsv", "*.list", "*.sample", "*.db", "*.txt", "*.gene_info", "*.xml", "*.dat", "gene_history", "stopwords_list1_lextek"],},
      )

