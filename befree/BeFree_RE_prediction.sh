#!/bin/bash

####################################################################
#
#	 Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.
#
#	 BeFree is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BeFree is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#	 How to cite BeFree:
#	 Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
#	 Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.
#
####################################################################

#######################
#     PARAMETERS      #
#######################
# Change these paths
DIRPATH="/home/../befree"
EXAMPLE_FILE_PATH="$DIRPATH/corpora/EUADR_JSRE_models/EUADR_target_disease.jsre"
MODEL_FILE_PATH="$DIRPATH/out/EUADR_target_disease_BEFREE.model"
PRED_FILE_PATH="$DIRPATH/out/prediction.txt"

#######################
#      SCRIPTS        #
#######################
JAR_PATH="$DIRPATH/src/re/BeFreeREPrediction.jar"

#######################
LOG_CONF="$DIRPATH/src/re/log-config.txt"
KERNEL_CONF="$DIRPATH/src/re/jsre-config.xml"

#######################
#     EXECUTION       #
#######################

JAVA="java"

$JAVA -jar $JAR_PATH -l $LOG_CONF -g $KERNEL_CONF $EXAMPLE_FILE_PATH $MODEL_FILE_PATH $PRED_FILE_PATH