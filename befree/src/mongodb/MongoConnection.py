# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import pymongo
from .mongodb_constants import MONGODB_IP, MONGODB_PORT

class MongoConnection(object):    
    def __init__(self, db_name, col_name,ip_adrr = MONGODB_IP, port = MONGODB_PORT,
                 auth = 0, user = None, pwd = None):
        
        self._superclass_init(db_name, col_name, ip_adrr, port, auth, user, pwd)
                
    def _superclass_init(self, db_name, col_name,ip_adrr = MONGODB_IP,
                         port = MONGODB_PORT, auth = 0, user = None, pwd = None):
        # Making a Connection
        self.connection = pymongo.MongoClient(ip_adrr, port)
        # Getting a Database
        self.database = self.connection.__getattr__(db_name)
        # Authentication
        if auth:
            if self.database.authenticate(user, pwd):
                self.collection = self.database.__getattr__(col_name)
            self.collection = None
        else:
            self.collection = self.database.__getattr__(col_name)
    
    def find(self, query = {}):
        return self.collection.find(query, skip=0, limit=0, no_cursor_timeout=False, snapshot=False)
    
    def find2(self, query = {}, fields=None, skip=0, limit=0, timeout=False, snapshot=False, tailable=False, _sock=None, _must_use_master=False, _is_command=False):
        return self.collection.find(query, projection=fields, skip=skip, limit=limit, no_cursor_timeout=timeout, snapshot=snapshot)
    
    def find_one(self, query = {}):
        return self.collection.find_one(query)
                
    def get_collection(self):
        return self.collection
    
    def set_collection(self, col_name):
        self.collection = self.database.__getattr__(col_name)

    def insert_document(self, document):
        # updates if exists; inserts if new
        self.collection.save(document)
    
    def delete_document(self, query = {}):
        # If query = {} --> remove all collection
        self.collection.remove(query)
    
    def delete_collection(self):
        self.collection.drop()
    
    def create_index(self, field, direction=pymongo.ASCENDING):
        #ensure_index creates the index if it does not exist
        self.collection.create_index(field)
    
    def close(self):
        self.connection.close()
    
    def copy_collection(self, new_conn):
        res = self.find()
        for rec in res:
            new_conn.insert_document(rec)
    
    def has_index(self, field):
        col_name = self.collection.full_name
        print(col_name)
        index_col = self.database.__getattr__("system.indexes")
        if index_col.find_one({"ns":col_name, "name":field}):
            return True
        return False
