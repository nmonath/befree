# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

from tqdm import tqdm
import sys

from dict_constants import GENE_DB_NAME, PARAM_COLL, ID, XREF_FIELD, HGNC,\
    ENTREZ_GENE, UNIPROT, XREF_FROM_DB, XREF_COLL, CROSS_REF, VER,DICT_COLL

current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from progressbar import ProgressBar, Percentage, Bar


def get_max_xref():
    res = MongoConnection(GENE_DB_NAME, PARAM_COLL).find_one({ID:0})
    return int(res.get("num_xref"))

def merge_cross_references(dbs):    
    collections = ""
    """
    collections = "_"
    if ENTREZ_GENE in dbs:
        collections = collections + "N"
    if UNIPROT in dbs:
        collections = collections + "U"    
    if HGNC in dbs:
        collections = collections + "H"
    """
    
    xref_conn = MongoConnection(GENE_DB_NAME, CROSS_REF + collections + VER)
    xref_conn.delete_collection()
    
    xref_conn.create_index(XREF_FIELD)
    xref_conn.create_index(UNIPROT + ID)
    xref_conn.create_index(ENTREZ_GENE + ID)
    xref_conn.create_index(HGNC + ID)
    
    
    if ENTREZ_GENE in dbs:
        gene_xref = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + XREF_COLL + collections + VER)
    if UNIPROT in dbs:
        uniprot_xref = MongoConnection(GENE_DB_NAME, UNIPROT + XREF_COLL + collections + VER)      
    if HGNC in dbs:
        hgnc_xref = MongoConnection(GENE_DB_NAME, HGNC + XREF_COLL + collections + VER)    
    
    size = get_max_xref()
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = size).start()
    progvar = 0
    
    
    for i in range(0, size):
        progvar += 1
        progress.update(progvar)
        ok = False
        
        doc = {}
        if ENTREZ_GENE in dbs:
            res_gene = gene_xref.find({XREF_FIELD:i})
            gene_list = []
            for r in res_gene:
                gene_list.append(r.get(ENTREZ_GENE + ID))
            if len(gene_list):
                doc[ENTREZ_GENE + ID] = gene_list
                
        if UNIPROT in dbs:
            res_uniprot = uniprot_xref.find({XREF_FIELD:i})
            uniprot_list = []
            for r in res_uniprot:
                uniprot_list.append(r.get(UNIPROT + ID))
            if len(uniprot_list):
                doc[UNIPROT + ID] = uniprot_list
                  
        if HGNC in dbs:
            res_hgnc = hgnc_xref.find({XREF_FIELD:i})
            hgnc_list = []
            for r in res_hgnc:
                hgnc_list.append(r.get(HGNC + ID))
            if len(hgnc_list):
                doc[HGNC + ID] = hgnc_list
        
        doc[XREF_FIELD] = i
        
        xref_conn.insert_document(doc)
    progress.finish()
    
def get_num_xref_from_parameters(param_conn):
    param = param_conn.find_one({ID:0})
    num_xref = int(param.get("num_xref"))
    param["num_xref"] = int(num_xref + 1)
    param_conn.insert_document(param)
    return int(num_xref)    

def init_collection(dbtype, collections):
    xref_conn = MongoConnection(GENE_DB_NAME, dbtype + XREF_COLL + collections + VER)
    xref_conn.delete_collection()
    xref_conn = MongoConnection(GENE_DB_NAME, dbtype + XREF_COLL + collections + VER)
    xref_conn.create_index(XREF_FIELD)
    xref_conn.create_index(dbtype + ID)
    return xref_conn
    
def create_xref_collections(dbs):
    
    current_path = sys.path[0]
    link_gene_log_path = "/".join(current_path.split("/")[:-2]) + "/out/link_gene.log"
    ofile = open(link_gene_log_path, "w")

    # INIT COLLECTIONS
    collections = ""
    """
    collections = "_"
    if ENTREZ_GENE in dbs:
        collections = collections + "N"
    if UNIPROT in dbs:
        collections = collections + "U"    
    if HGNC in dbs:
        collections = collections + "H"
    """
    valid_fields = {}
    gene_connections = {}
    if ENTREZ_GENE in dbs:
        valid_fields[ENTREZ_GENE] = init_collection(ENTREZ_GENE,collections)
        gene_connections[ENTREZ_GENE] = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + VER)
    if UNIPROT in dbs:
        valid_fields[UNIPROT] = init_collection(UNIPROT,collections)
        gene_connections[UNIPROT] = MongoConnection(GENE_DB_NAME, UNIPROT + VER)
    if HGNC in dbs:
        valid_fields[HGNC] = init_collection(HGNC,collections)
        gene_connections[HGNC] = MongoConnection(GENE_DB_NAME, HGNC + VER)
    
    param_conn = MongoConnection(GENE_DB_NAME, PARAM_COLL)    
    param_conn.insert_document({"_id":0, "num_xref":0})
    
    if ENTREZ_GENE in dbs:
        print("1", "NCBI Gene....")
        link_genes(ENTREZ_GENE, valid_fields, gene_connections, ofile)
    if UNIPROT in dbs:
        print("2", "UNIPROT...")
        link_genes(UNIPROT, valid_fields, gene_connections, ofile)
    if HGNC in dbs:
        print("3", "HGNC...")
        link_genes(HGNC, valid_fields, gene_connections, ofile)
    
    ofile.close()

def link_genes(dbtype, xref_connections, gene_connections, ofile):
    
    param_conn = MongoConnection(GENE_DB_NAME, PARAM_COLL)
    gene_xref_from_conn = MongoConnection(GENE_DB_NAME, dbtype + XREF_FROM_DB + VER)
    
    gene_list = gene_connections.get(dbtype).collection.find({}, no_cursor_timeout=True).batch_size(10)
    
    ofile.write("START!!! " + dbtype + " --> " + str(gene_list.count()) + "\n")
    
    file_len = gene_list.collection.count_documents({})
    #progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    for gene in tqdm(gene_list, total=file_len):
        progvar+=1
        #progress.update(progvar)
        # Get gene ID from record
        gene_id = gene.get(dbtype + ID)
        ofile.write("  " + gene_id + "\n")
        
        # Exists gene in [gene]_xref???
        gene_rec = xref_connections.get(dbtype).find_one({dbtype + ID:gene_id})
        
        if gene_rec:
            # Yes! -> Get nlink!
            nlink = gene_rec.get(XREF_FIELD)
            ofile.write("    " + "EXISTS IN XREF --> NLINK = " + str(nlink) + "\n")
        else:
            # No! -> Make new nlink!
            nlink = get_num_xref_from_parameters(param_conn)
            doc = {}
            doc[dbtype + ID] = gene_id
            doc[XREF_FIELD] = int(nlink)
            # Insert new xreference with new nlink in [gene]_xref
            xref_connections.get(dbtype).insert_document(doc)
            ofile.write("    " + "NOT EXISTS IN XREF --> NLINK = " + str(nlink) + "\n")
        
        # Exists gene in [gene]_xref_from_db???
        gene_rec_from = gene_xref_from_conn.find_one({dbtype + ID:gene_id})
        if gene_rec_from:
            # Yes! -> There are xreferences!!!
            ofile.write("    " + "EXISTS IN XREF_FROM_DB" + "\n")
            for fields in gene_rec_from.items():
                # Get resource of reference
                ref_type = fields[0][:-3]
                # Is a resource valid??
                if ref_type in xref_connections and ref_type != dbtype:
                    # Yes! -> uniprot, gene or hgnc.
                    ofile.write("      " + "VALID TYPE --> " + ref_type + "\n")
                    # Get IDs from resource
                    for f in fields[1].split("|"):
                        res_gene = gene_connections.get(ref_type).find_one({ref_type + ID:f})
                        ofile.write("        " + "ID = " + f + "\n")
                        # Is this ID in database? 
                        if res_gene:
                            # Yes!!!
                            ofile.write("        " + "EXISTS IN " + ref_type + " collection" + "\n")
                            xref_res = xref_connections.get(ref_type).find_one({ref_type + ID:f})
                            # Exists this xreference in [gene]_xref???
                            if xref_res:
                                # Yes! -> Get old nlink
                                nlink_ref = int(xref_res.get(XREF_FIELD))
                                ofile.write("          " + "EXISTS IN " + ref_type + "_xref collection NLINK = " + str(nlink_ref) + "\n")
                                # Is this old_nlink different?
                                if nlink_ref != nlink:
                                    # Yes -> Update: Change all old nlink by new nlink
                                    for key in xref_connections.keys():
                                        ofile.write("          " + "UPDATE IN " + key + "\n")
                                        #xref_connections.get(key).get_collection().update({XREF_FIELD:nlink_ref}, {"$set": {XREF_FIELD:nlink}})
                                        ref_rec = xref_connections.get(key).find({XREF_FIELD:nlink_ref})                                        
                                        for rec in ref_rec:
                                            ofile.write("            " + rec[key + ID] + "\n")
                                            rec[XREF_FIELD] = int(nlink)
                                            xref_connections.get(key).insert_document(rec)
                            else:
                                # No! -> Make new xreference!
                                doc = {}
                                doc[ref_type + ID] = f
                                doc[XREF_FIELD] = int(nlink)
                                xref_connections.get(ref_type).insert_document(doc)
                                ofile.write("          " + "NOT EXISTS IN " + ref_type + "_xref collection NLINK = " + str(nlink) + "\n")
    # progress.finish()
    gene_list.close()  

def test_counts():
    collections = ""
    """
    collections = "_"
    if ENTREZ_GENE in dbs:
        collections = collections + "N"
    if UNIPROT in dbs:
        collections = collections + "U"    
    if HGNC in dbs:
        collections = collections + "H"
    """    
    hgnc_xref = MongoConnection(GENE_DB_NAME, HGNC + XREF_COLL + collections + VER)
    uniprot_xref = MongoConnection(GENE_DB_NAME, UNIPROT + XREF_COLL + collections + VER)
    gene_xref = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + XREF_COLL + collections + VER)
    
    
    
    
    
    print("COUNTS REF")
    print("---------------------------------")
    print("    HGNC:    ", hgnc_xref.get_collection().count())
    print("    UNIPROT: ", uniprot_xref.get_collection().count())
    print("    GENE:    ", gene_xref.get_collection().count())
    print("")
    print("DISTINCT ID")
    print("---------------------------------")
    print("    HGNC:    ", len(hgnc_xref.get_collection().distinct(HGNC + ID)))
    print("    UNIPROT: ", len(uniprot_xref.get_collection().distinct(UNIPROT + ID)))
    print("    GENE:    ", len(gene_xref.get_collection().distinct(ENTREZ_GENE + ID)))
    print("")
    print("DISTINCT XREF")
    print("---------------------------------")
    print("    HGNC:    ", len(hgnc_xref.get_collection().distinct(XREF_FIELD)))
    print("    UNIPROT: ", len(uniprot_xref.get_collection().distinct(XREF_FIELD)))
    print("    GENE:    ", len(gene_xref.get_collection().distinct(XREF_FIELD)))
    print("")
    
    hgnc_xref = MongoConnection(GENE_DB_NAME, HGNC + DICT_COLL + VER)
    uniprot_xref = MongoConnection(GENE_DB_NAME,UNIPROT + DICT_COLL + VER)
    gene_xref = MongoConnection(GENE_DB_NAME, ENTREZ_GENE+ DICT_COLL + VER)
    
    print("COUNTS DICT")
    print("---------------------------------")
    print("    HGNC:    ", hgnc_xref.get_collection().count())
    print("    UNIPROT: ", uniprot_xref.get_collection().count())
    print("    GENE:    ", gene_xref.get_collection().count())
    print("")
    print("DISTINCT ID")
    print("---------------------------------")
    print("    HGNC:    ", len(hgnc_xref.get_collection().distinct(HGNC + ID)))
    print("    UNIPROT: ", len(uniprot_xref.get_collection().distinct(UNIPROT + ID)))
    print("    GENE:    ", len(gene_xref.get_collection().distinct(ENTREZ_GENE + ID)))
                                    
                                    

def get_geneid2xref_dict(gene_list = []):
    geneid_dict_xref = {}
    xref_conn = MongoConnection(GENE_DB_NAME, CROSS_REF + VER)
    
    if len(gene_list):
        res = xref_conn.find({ENTREZ_GENE + ID:{"$in":gene_list}})
    else:
        res = xref_conn.find()
    for rec in res:
        gene_id_list = rec.get(ENTREZ_GENE + ID)
        if gene_id_list:
            xref = rec.get(XREF_FIELD)
            for gene_id in gene_id_list:
                geneid_dict_xref[gene_id] = xref
    return geneid_dict_xref


if __name__ == '__main__':
    
    dbs = []
    dbs.append(ENTREZ_GENE)
    dbs.append(UNIPROT)
    dbs.append(HGNC)
    
    create_xref_collections(dbs)
    merge_cross_references(dbs)
    
    #test_counts()
