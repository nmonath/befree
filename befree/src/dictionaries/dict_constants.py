# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

VER = ""
GENE_DB_NAME = "gene"
DICT_DB_NAME = "dictionary"

DICT_GENE = "gene"
DICT_DISEASE = "disease"

#########################
# DICTIONARY REPOSITORY #
#########################
INFO_COLLECTION = "info"
DICT_RAW = "_raw"
DICT_COLL_GROUP = "_dict_grouped"
DICT_COLL = "_dict"
DICT_TERM = "term"
DICT_SYMBOL = "symbol"
DICT_EC = "ec"
DICT_PLURAL = "plural"
DICT_ID = "concept_id"
DICT_ORIGINAL = "original"
DICT_TUI = "tui"
DICT_VER = "ver"
DICT_PLURAL = "plural"
DICT_LOG = "_log"
GENE = "gene"
DISEASE = "disease"


####


#PROJECT_PATH = "/home/abravo/workspace/DictionaryRepository/"
#INPUT_PATH = PROJECT_PATH + "in/"
#OUTPUT_PATH = PROJECT_PATH + "out/"

#DISEASE_INPUT_FILE = INPUT_PATH + "diccionaryDiseaseRestr_2013.txt" #"diseases_dictionary.txt"

#STOPWORDS = INPUT_PATH + "stopwords"




#DICT_DISEASE_CASPER = DICT_DISEASE + "_casper"
#DICT_DISEASE_BC5 = DICT_DISEASE + "_casper_BC5"
#DICT_DISEASE_BC5_T046 = DICT_DISEASE_BC5 + "_T046"
#DICT_DISEASE_BC5_ALL = DICT_DISEASE_BC5 + "_ALL"
#DICT_CELL_LINE = "cell_line"
#DICT_DISEASE_PSYGENET2015 = DICT_DISEASE_CASPER + "_psy15"

####
STP = "_stp"
LEN = "_ln"
RAW = "_raw"
LOW = "_low"
SPL = "_spl"
CHR = "_chr"
PMK = "_pmk"
ORD = "_ord"
DPK = "_dpk"
GPK = "_gpk"
SET = "_set"
REP = "_rep"
REN = "_ren"

####################
# GENES REPOSITORY #
####################

# NCBI_GENE DATABASE
ENTREZ_GENE = "gene"
HGNC = "hgnc"
VEGA = "vega"
MIRBASE = "mirbase" 
ENSEMBL = "ensembl"
OMIM = "omim"
RGD = "rgd"
HPRD = "hprd"
IMGT = "imgt" # IMGT/GENE-DB the international ImMunoGeneTics information system for immunoglobulins
ENZYME = "enzyme"
PUBMED = "pubmed"
REFSEQ = "refseq"
CCDS = "ccds"
GDB = "gdb"
UNIPROT = "uniprot"
UCSC = "ucsc"
MGI = "mgi" # Mouse Genome Database
RGD = "rgd" # Rat Genome Database
HORDE = "horde" # HORDE ID Human Olfactory Receptor Data Exploratorium
CD = "cd" # CD Human Cell Differentiation Antigens
RFAM = "rfam" # Rfam RNA families database of alignments and CMs
SNORNABASE = "snornabase" # snoRNABase database of human H/ACA and C/D box snoRNAs
KZNF = "kznf" # KZNF Gene Catalog Human KZNF Gene Catalog
IFDB = "ifdb" # Intermediate Filament DB Human Intermediate Filament Database
IUPHAR = "iuphar" # IUPHAR Committee on Receptor Nomenclature and Drug Classification.(mapped)
MEROPSTHE = "meropsthe" # MEROPSthe peptidase database
COSMIC = "cosmic" # COSMIC Catalogue Of Somatic Mutations In Cancer
ORPHANET = "orphanet" # Orphanet portal for rare diseases and orphan drugs
PSEUDOGENE = "pseudogene" # Pseudogene.org database of identified pseudogenes
PIRNABANK = "pirnabank" # piRNABank database of piwi-interacting RNA clusters
HOMEO = "homeo" # HomeoDB a database of homeobox gene diversity
MAMIT = "mamit" # Mamit-tRNAdb a compilation of mammalian mitochondrial tRNA
                # genes
# UNIPROT
UNIPROTID = UNIPROT + "ID"
GI = "gi"
PDB = "pdb"
GO = "go"
IPI = "ipi"
UNIREF100 = "uniref100"
UNIREF90 = "uniref90"
UNIREF50 = "uniref50"
UNIPARC = "uniparc"
PIR = "pir"
MIM = OMIM
UNIGENE = "unigene"
EMBL = "embl"
EMBL_CDS = EMBL + "_cds"
ENSEMBL_TRS = ENSEMBL + "_trs"
ENSEMBL_PRO = ENSEMBL + "_pro"
PUBMED_ADDITIONAL = PUBMED + "_additional"

#SRC_DIR = PROJECT_PATH + "src/"
#OUT_DIR = PROJECT_PATH + "out/"
#IN_DIR = PROJECT_PATH + "in/"

PARAM_COLL = "parameters"
CROSS_REF = "cross_references"
XREF_FIELD = "xref"
XREF_COLL = "_" + XREF_FIELD
ID = "_id"
XREF_FROM_DB = XREF_COLL + "_from_db"
HISTORIC = "_historic"

# FILES
# UNIPROT
UNIPROT_FILE_NAME_MAPPING = "idmapping_selected.tab"
UNIPROT_FILE_NAME_MAPPING_9606 = "HUMAN_9606_idmapping_selected.tab"
UNIPROT_FILE_NAME_SPROT = "uniprot_sprot_human.dat"
# ENTREZ
ENTREZ_FILE_NAME = "gene_info"
ENTREZ_FILE_NAME_9606 = "gene_info_9606"
ENTREZ_FILE_HISTORIC = "gene_history"
# HGNC
#HGNC_FILE_NAME = "hgnc_aproved"
HGNC_FILE_NAME = "hgnc_complete_set.txt"


#######################
#      FILE INFO      #
#######################

#HGNC COLUMNS
"""
COL_HGNC_FILE_ID = 0
COL_HGNC_FILE_Approved_Symbol = 1
COL_HGNC_FILE_Approved_Name = 2
COL_HGNC_FILE_Status = 3
COL_HGNC_FILE_Previous_Symbols = 4
COL_HGNC_FILE_Previous_Name = 5
COL_HGNC_FILE_Synonyms = 6
COL_HGNC_FILE_Name_Synonyms = 7
COL_HGNC_FILE_Enzyme_IDs = 8
COL_HGNC_FILE_Entrez_Gene_ID = 9
COL_HGNC_FILE_Entrez_Gene_ID_Supp = 10
COL_HGNC_FILE_UniProt_ID = 11
"""
#NEW version file: ftp://ftp.ebi.ac.uk/pub/databases/genenames/hgnc_complete_set.txt.gz
COL_HGNC_FILE_ID = 0
COL_HGNC_FILE_Approved_Symbol = 1
COL_HGNC_FILE_Approved_Name = 2
COL_HGNC_FILE_Status = 5
COL_HGNC_FILE_Previous_Symbols = 10
COL_HGNC_FILE_Previous_Name = 11
COL_HGNC_FILE_Synonyms = 8
COL_HGNC_FILE_Name_Synonyms = 9
COL_HGNC_FILE_Enzyme_IDs = 46
COL_HGNC_FILE_Entrez_Gene_ID = 18
#COL_HGNC_FILE_Entrez_Gene_ID_Supp = 33
COL_HGNC_FILE_UniProt_ID = 25

#NCBI COLUMNS
COL_NCBI_FILE_tax_id = 0
COL_NCBI_FILE_gene_id = 1
COL_NCBI_FILE_symbol = 2
COL_NCBI_FILE_synonyms = 4
COL_NCBI_FILE_description = 8
COL_NCBI_FILE_symbol_from_nomen = 10
COL_NCBI_FILE_full_from_nomen = 11
COL_NCBI_FILE_other_designations = 13

#UNIPROT COLUMNS
COL_UNIPROT_NCBI_FILE_Uniprot_ID = 0
COL_UNIPROT_NCBI_FILE_NCBI_Gene_ID = 2

#UMLS COLUMNS
#cui    str    TUI    TTY
COL_UMLS_FILE_cui = 0
COL_UMLS_FILE_str = 1
COL_UMLS_FILE_tui = 2
COL_UMLS_FILE_tty = 3
