# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys
from dict_constants import DICT_DB_NAME, DICT_GENE, DICT_ID, DICT_TERM, DICT_VER,\
    GENE_DB_NAME, CROSS_REF, HGNC, ID, UNIPROT, ENTREZ_GENE, XREF_FIELD, VER,\
    DICT_COLL
from normalization import gene_normalization
from progressbar import ProgressBar, Percentage, Bar    
from gene_cross_references import create_xref_collections,\
    merge_cross_references, test_counts
current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection

def get_ambiguity(dict_name):
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    all_dict = dict_conn.find({})
    
    N = dict_conn.get_collection().count()
    sum = 0
    for rec1 in all_dict:
        term1 = rec1.get(DICT_TERM)
        records = dict_conn.find({DICT_TERM:term1})
        id_dict = {}
        for rec2 in records:
            id_dict[rec2.get(DICT_ID)] = 1
        sum += len(id_dict.keys())
        
    print(dict_name + " ambiguity:", sum/float(N))

def get_variability(dict_name):  
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    all_dict = dict_conn.find({})
    ids = {}
    for rec in all_dict:
        ids[rec.get(DICT_ID)] = 1
    M = len(ids.keys())
    sum = 0
    for k in ids.keys():
        records = dict_conn.find({DICT_ID:k})
        rec_dict = {}
        for rec in records:
            rec_dict[rec.get(DICT_TERM)] = 1
        sum += len(rec_dict.keys())
    print(dict_name + " variability:" ,sum/float(M))
       
    
def dictionary_creation(dbs):
    print("Gene dictionary compilation...")
    collections = ""
    
    if ENTREZ_GENE in dbs:
        #collections = collections + "N"
        gene_dict = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + DICT_COLL + VER)
    if UNIPROT in dbs:
        #collections = collections + "U"
        uniprot_dict = MongoConnection(GENE_DB_NAME, UNIPROT + DICT_COLL + VER)
    if HGNC in dbs:
        #collections = collections + "H"
        hgnc_dict = MongoConnection(GENE_DB_NAME, HGNC + DICT_COLL + VER)
    
    
    
    xref_conn = MongoConnection(GENE_DB_NAME, CROSS_REF + collections + VER)
    
    dict_coll = MongoConnection(DICT_DB_NAME, DICT_GENE + collections + VER)
    dict_coll.delete_collection()
    dict_coll.create_index(DICT_ID)
    dict_coll.create_index(DICT_TERM)
    dict_coll.create_index(DICT_VER)
    
    res_xref =  xref_conn.collection.find({}, no_cursor_timeout=True)
    
    file_len = res_xref.collection.count_documents({})
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    for rec in res_xref:
        progvar+=1
        progress.update(progvar)
        num_xref = rec.get(XREF_FIELD)
        
        if ENTREZ_GENE in dbs:
            gene_list =  rec.get(ENTREZ_GENE + ID)
            if gene_list:
                for gene in gene_list:
                    term_res = gene_dict.find({ENTREZ_GENE + ID:gene})
                    if term_res.collection.count_documents({}):
                        for t in term_res:
                            t.pop(ID)
                            t.pop(ENTREZ_GENE + ID)
                            t[DICT_ID] = num_xref
                            t[DICT_VER] = VER
                            dict_coll.insert_document(t)
        
        if UNIPROT in dbs:
            uniprot_list = rec.get(UNIPROT + ID)
            if uniprot_list:
                for gene in uniprot_list:
                    term_res = uniprot_dict.find({UNIPROT + ID:gene})
                    if term_res.collection.count_documents({}):
                        for t in term_res:
                            t.pop(ID)
                            t.pop(UNIPROT + ID)
                            t[DICT_ID] = num_xref
                            t[DICT_VER] = VER
                            dict_coll.insert_document(t)
                            
        if HGNC in dbs:
            hgnc_list = rec.get(HGNC + ID)
            if hgnc_list:
                for gene in hgnc_list:
                    term_res = hgnc_dict.find({HGNC + ID:gene})
                    if term_res.collection.count_documents({}):
                        for t in term_res:
                            t.pop(ID)
                            t.pop(HGNC + ID)
                            t[DICT_ID] = num_xref
                            t[DICT_VER] = VER
                            dict_coll.insert_document(t)
    dict_coll.close()
    res_xref.close()
    progress.finish() 
    print("Done!")
    
                    
def test(dbs):
    collections = ""
    """
    if ENTREZ_GENE in dbs:
        collections = collections + "N"
    if UNIPROT in dbs:
        collections = collections + "U"    
    if HGNC in dbs:
        collections = collections + "H"
    """
    coll = DICT_GENE + collections
    dict_coll = MongoConnection(DICT_DB_NAME, coll)
    print(len(dict_coll.get_collection().distinct(DICT_ID)))
    
    get_ambiguity(coll)
    get_variability(coll)
    


def testing(coll_name):
    print(coll_name)
    dict_coll = MongoConnection(DICT_DB_NAME, coll_name)
    print(len(dict_coll.get_collection().distinct(DICT_ID)))
    get_ambiguity(coll_name)
    get_variability(coll_name)
    
    

def count_unique_terms(coll_name):
    dict_coll = MongoConnection(DICT_DB_NAME, coll_name)
    records = dict_coll.find()
    term_dict = {}
    for rec in records:
        term_dict[rec.get(DICT_TERM)] = 1
    
    print(coll_name + ":" ,len(term_dict))

if __name__ == '__main__':
    
    dbs = []
    dbs.append(ENTREZ_GENE)
    dbs.append(UNIPROT)
    dbs.append(HGNC)
    
    
    print("Creating Cross-reference collections...")
    create_xref_collections(dbs)
    
    print("Merging Cross-reference collections...")
    merge_cross_references(dbs)
    test_counts()
    
    dictionary_creation(dbs)
    #testing(DICT_GENE + VER)
    
    print("Gene Normalization Process...")
    gene_normalization(DICT_GENE)
    
    
    #dict_name = "gene_NUH_chr_ln1_ln2_low_spl_rom_rep_set_pmk"
    #print "Add Greek Letters"
    #dict_name = add_greek_letters(dict_name)
