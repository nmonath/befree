# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, csv, re
current_path = sys.path[0]
medline_path = "/".join(current_path.split("/")[:-1]) + "/ner"
sys.path.append(medline_path)

from BeFree_utils import add_elem_dictionary

COL_ASSOCIATION_TYPE = 0
COL_PMID = 1
COL_NUM_SENTENCE = 2
COL_ENTITY1_TEXT = 3
COL_ENTITY1_INI = 4
COL_ENTITY1_END = 5
COL_ENTITY1_TYPE = 6
COL_ENTITY2_TEXT = 7
COL_ENTITY2_INI = 8
COL_ENTITY2_END = 9
COL_ENTITY2_TYPE = 10
COL_SENTENCE = 11
COL_ENTITY1_OFFSET_LIST = 12
COL_ENTITY2_OFFSET_LIST = 13

def cooc_creation_2_jsre(input_path, output_path, ent1_A, ent1_B, ent2_A, ent2_B):
    
    #20967760    2011    Hepatology    1527-3350    TITLE    0    0    246    LONGTERM|GENE|GN|DICTIONARY    12 15 lipoxygenase    12/15-lipoxygenase    18#36    NA    C0400966    LONGTERM|DISEASE|DT|DICTIONARY    nonalcoholic fatty liver disease    nonalcoholic fatty liver disease    85#117    NA    Disruption of the 12/15-lipoxygenase gene (Alox15) protects hyperlipidemic mice from nonalcoholic fatty liver disease.    18#36|43#49    85#117    
    
    ENT1_A = "€".decode('utf-8')
    ENT1_B = "$".decode('utf-8')
    ENT2_A = "¿".decode('utf-8')
    ENT2_B = "¬".decode('utf-8')
    
    
    ofile = open(output_path, "w")
    
    for lin in file(input_path):
        lin_split = lin.strip().split("\t")
        
        offset1 = lin_split[COL_ENTITY1_INI] + "#"+ lin_split[COL_ENTITY1_END]
        offset2 = lin_split[COL_ENTITY2_INI] + "#"+ lin_split[COL_ENTITY2_END]
        
        
        offset1_list = lin_split[COL_ENTITY1_OFFSET_LIST].split("|")
        offset2_list = lin_split[COL_ENTITY2_OFFSET_LIST].split("|")
        
        sent_prev = lin_split[COL_SENTENCE]
        sent = sent_prev#decode('utf-8')
        sent_2 = sent
        
        for off1 in offset1_list:
            ini = int(off1.split("#")[0])
            end = int(off1.split("#")[1])
            
            size = end - ini
            sent_ini = sent[:ini]
            sent_end = sent[end:]
            
            ent1 = "".join([ENT1_B]*size)
            if off1 == offset1:
                ent1 = "".join([ENT1_A]*size)
            sent = sent_ini + ent1 + sent_end 
            
            #sent2
            sent_2_ini = sent_2[:ini]
            sent_2_end = sent_2[end:]
            sent_2 = sent_2_ini + sent_2[ini:end].replace(" ", "_") + sent_2_end 
        
        for off2 in offset2_list:
            ini = int(off2.split("#")[0])
            end = int(off2.split("#")[1])
            
            size = end - ini
            sent_ini = sent[:ini]
            sent_end = sent[end:]
            
            ent2 = "".join([ENT2_B]*size)
            if off2 == offset2:
                ent2 = "".join([ENT2_A]*size)
            sent = sent_ini + ent2 + sent_end 
            
            #sent2
            sent_2_ini = sent_2[:ini]
            sent_2_end = sent_2[end:]
            sent_2 = sent_2_ini + sent_2[ini:end].replace(" ", "_") + sent_2_end
        
        sent = re.sub(r'[€]+', ent1_A, sent)
        sent = re.sub(r'[$]+', ent1_B, sent)
        sent = re.sub(r'[¿]+', ent2_A, sent)
        sent = re.sub(r'[¬]+', ent2_B, sent)
        
        if not ent1_A in sent or not ent2_A in sent:
            continue
        
        lin_split.append(sent.encode('utf-8'))
        
        new_line = lin_split[0]
        for i in range(1, len(lin_split)):
            new_line = new_line + "\t" + lin_split[i]
        
        ofile.write(new_line + "\n")
                        
    ofile.close()   

def cooc_add_all_offsets_by_sentence(input_path, output_path):
    
    #11106199    2001    Eur. Respir. J.    0903-1936    TITLE    0    0    65797    LONGTERM|NUMBER|DICTIONARY    tumour necrosis factor alpha    tumour necrosis factor alpha    49#77    NA    C0004096    LONGTERM|DICTIONARY    asthma    asthma    34#40    NA    Lack of association between adult asthma and the tumour necrosis factor alpha-308 polymorphism gene.
    #assoc_dict = {}
    dis_offsets_dict = {}
    gene_offsets_dict = {}
    
    header = 1
    with open(input_path, 'rb') as f:
        reader = csv.reader(f, delimiter='\t')
        for lin_split in reader:
            if header:
                header=0
                continue
            pmid = lin_split[COL_PMID]
            num_sent = lin_split[COL_NUM_SENTENCE]
            sent_id = "-".join([pmid, num_sent])
            offset1 = lin_split[COL_ENTITY1_INI] + "#"+ lin_split[COL_ENTITY1_END]
            offset2 = lin_split[COL_ENTITY2_INI] + "#"+ lin_split[COL_ENTITY2_END]
            add_elem_dictionary(gene_offsets_dict, sent_id, offset1)
            add_elem_dictionary(dis_offsets_dict, sent_id, offset2)
        
    ofile = open(output_path, "w")
    header = 1
    with open(input_path, 'rb') as f:
        reader = csv.reader(f, delimiter='\t')
        for lin_split in reader:
            if header:
                header=0
                continue
            pmid = lin_split[COL_PMID]
            num_sent = lin_split[COL_NUM_SENTENCE]
            sent_id = "-".join([pmid, num_sent])
            lin_split.append("|".join(gene_offsets_dict[sent_id]))
            lin_split.append("|".join(dis_offsets_dict[sent_id]))
            ofile.write("\t".join(lin_split)+"\n")
    ofile.close()


if __name__ == '__main__':
    
    input_path = "/home/abravo/befree/corpora/EUADR_Corpus_IBIgroup/EUADR_target_disease.csv"
    output_path = "/home/abravo/befree/corpora/EUADR_JSRE_models/EUADR_target_disease_jsre"
    #cooc_add_all_offsets_by_sentence(input_path, output_path)
    
    
    input_path = "/home/abravo/befree/corpora/EUADR_JSRE_models/EUADR_target_disease_jsre"
    output_path = "/home/abravo/befree/corpora/EUADR_JSRE_models/EUADR_target_disease_jsre.jsre"
    ent1_A = 'GeneA'
    ent1_B = 'GeneB'
    ent2_A = 'DiseaseA'
    ent2_B = 'DiseaseB'
    #cooc_creation_2_jsre(input_path, output_path, ent1_A, ent1_B, ent2_A, ent2_B)
    
    #EUADR DRUG-TARGET
    input_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Corpus_IBIgroup/EUADR_drug_target.csv"
    output_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_target_jsre"
    #cooc_add_all_offsets_by_sentence(input_path, output_path)
    input_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_target_jsre"
    output_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_target_jsre.jsre"
    ent1_A = 'GeneA'
    ent1_B = 'GeneB'
    ent2_A = 'DrugA'
    ent2_B = 'DrugB'
    #cooc_creation_2_jsre(input_path, output_path, ent1_A, ent1_B, ent2_A, ent2_B)
    
    
    #EUADR DRUG-DISEASE
    input_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Corpus_IBIgroup/EUADR_drug_disease.csv"
    output_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_disease_jsre"
    cooc_add_all_offsets_by_sentence(input_path, output_path)
    input_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_disease_jsre"
    output_path = "/ibi/users/shared/BeFree_data/corpora/EUADR_Intermediate/EUADR_drug_disease_jsre.jsre"
    ent1_A = 'DiseaseA'
    ent1_B = 'DiseaseB'
    ent2_A = 'DrugA'
    ent2_B = 'DrugB'
    cooc_creation_2_jsre(input_path, output_path, ent1_A, ent1_B, ent2_A, ent2_B)
    
