# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import regex

DICT_GENE_NAME = "gene_final_merged"
DICT_DISEASE_NAME = "disease_final_merged"

#DOCUMENT INFO
YEAR_FIELD = "year"
JOURNAL_TITLE_FIELD = "journal_title"
JOURNAL_ABBRE_FIELD = "journal_abbre"
ISSN_FIELD = "issn"
MESH_FIELD = "mesh_list"
ID_FIELD = "_id"
LABEL_LIST_FIELD = "section_list"
ABSTRACT_LIST_FIELD = "section_text_list"
ABSTRACT_LIST_BY_SENT_FIELD = "section_text_by_sentences_list"

#DICTIONARIES
"""
DICT_COLL = "_raw"
DICT_TERM = "term"
DICT_SYMBOL = "symbol"
DICT_EC = "ec"
DICT_ID = "concept_id"
DICT_ORIGINAL = "original"
DICT_TUI = "tui"
DICT_VER = "ver"
DICT_PLURAL = "plural"
"""
# ENTITY TYPES
GENE_ENTITY = 1
DISEASE_ENTITY = 2
TOXICITY_ENTITY = 3
CELL_LINE_ENTITY = 4

# PATTERN CREATION TERM INFO
TERM_INFO_TOTAL = 4
TERM_INFO_CONCEPT_ID = 0
TERM_INFO_SYMBOL = 1
TERM_INFO_EC = 2
TERM_INFO_PLURAL = 3

# ACRONYM EXTRACTION INFO
ACRONYM_INFO_TOTAL = 3
ACRONYM_INFO_PATTERN = 0
ACRONYM_INFO_CONCEPT_ID = 1
ACRONYM_INFO_ORIGIN = 2

# RESULTS FOR ENTITIES
# Document Info
RESULT_DOCID = 0
RESULT_YEAR = 1
RESULT_JOURNAL = 2
RESULT_ISSN = 3
# Sentence Info
RESULT_SECTION = 4
RESULT_SECTION_NUM = 5
RESULT_SENT_NUM = 6
# Entity Info
RESULT_ENTITY_ID = 7
RESULT_ENTITY_TYPE = 8
RESULT_ENTITY_NORM = 9
RESULT_ENTITY_TEXT = 10
RESULT_ENTITY_OFFSET = 11
RESULT_ENTITY_PARENT = 12
# Sentence
RESULT_SENT_TEXT = 13

# RESULTS FOR CO-OCCURRENCES
# Document Info
RESULT_COOC_DOCID = 0
RESULT_COOC_YEAR = 1
RESULT_COOC_JOURNAL = 2
RESULT_COOC_ISSN = 3
# Sentence Info
RESULT_COOC_SECTION = 4
RESULT_COOC_SECTION_NUM = 5
RESULT_COOC_SENT_NUM = 6
# Entity1 Info
RESULT_COOC_ENTITY1_ID = 7
RESULT_COOC_ENTITY1_TYPE = 8
RESULT_COOC_ENTITY1_NORM = 9
RESULT_COOC_ENTITY1_TEXT = 10
RESULT_COOC_ENTITY1_OFFSET = 11
RESULT_COOC_ENTITY1_PARENT = 12
# Entity2 Info
RESULT_COOC_ENTITY2_ID = 13
RESULT_COOC_ENTITY2_TYPE = 14
RESULT_COOC_ENTITY2_NORM = 15
RESULT_COOC_ENTITY2_TEXT = 16
RESULT_COOC_ENTITY2_OFFSET = 17
RESULT_COOC_ENTITY2_PARENT = 18
# Sentence
RESULT_COOC_SENT_TEXT = 19

RESULT_COOC_ENTITY1_OFFSET_LIST = 20
RESULT_COOC_ENTITY2_OFFSET_LIST = 21

RESULT_COOC_SENT_TEXT_BEFORE_JSRE = 22

GENE_PATTERN_LIST = []
GENE_PATTERN_LIST.append("gene")
GENE_PATTERN_LIST.append("protein")
GENE_PATTERN_LIST.append("factor")
GENE_PATTERN_LIST.append("receptor")
GENE_PATTERN_LIST.append("enzyme")
GENE_PATTERN_LIST.append("target")
GENE_PATTERN_LIST.append("biomarker")
GENE_PATTERN_LIST.append("biological marker")
GENE_PATTERN_LIST.append("biologic marker")
GENE_PATTERN_LIST.append("clinical marker")
GENE_PATTERN_LIST.append("immune marker")
GENE_PATTERN_LIST.append("immunologic marker")
GENE_PATTERN_LIST.append("viral marker")
GENE_PATTERN_LIST.append("serum marker")
GENE_PATTERN_LIST.append("surrogate marker")
GENE_PATTERN_LIST.append("biochemical marker")
GENE_PATTERN_LIST.append("laboratory marker")
GENE_PATTERN_LIST.append("marker")
# aBravo 23.12.2014
#GENE_PATTERN_LIST.append("mutation")
GENE_PATTERN_LIST.append("promoter")

GENE_PATTERN = r'[\s]{,2}('+'|'.join(sorted(GENE_PATTERN_LIST))+')[s]?'
GENE_PATTERN_REGEX = regex.compile(GENE_PATTERN)

GENE_FAMILY_PATTERN_LIST = []
GENE_FAMILY_PATTERN_LIST.append("unit")
GENE_FAMILY_PATTERN_LIST.append("subunit")
GENE_FAMILY_PATTERN_LIST.append("cdna")
GENE_FAMILY_PATTERN_LIST.append("mrna")
GENE_FAMILY_PATTERN_LIST.append("cDNA")
GENE_FAMILY_PATTERN_LIST.append("mRNA")
GENE_FAMILY_PATTERN_LIST.append("family")
GENE_FAMILY_PATTERN_LIST.append("families")
GENE_FAMILY_PATTERN_LIST.append("subfamily")
GENE_FAMILY_PATTERN_LIST.append("subfamilies")
GENE_FAMILY_PATTERN_LIST.append("subfamily")
GENE_FAMILY_PATTERN_LIST.append("subtype")
GENE_FAMILY_PATTERN_LIST.append("type")


GENE_FAMILY_PATTERN = r'[\s]{,2}('+'|'.join(sorted(GENE_FAMILY_PATTERN_LIST))+')'
GENE_FAMILY_PATTERN_REGEX = regex.compile(GENE_FAMILY_PATTERN)

DISEASE_PATTERN_LIST = []
DISEASE_PATTERN_LIST.append("disease")
DISEASE_PATTERN_LIST.append("syndrome")
DISEASE_PATTERN_LIST.append("symptom")
DISEASE_PATTERN_LIST.append("disorder")
DISEASE_PATTERN_LIST.append("phenotype")
DISEASE_PATTERN_LIST.append("infection")
DISEASE_PATTERN_LIST.append("condition")
DISEASE_PATTERN_LIST.append("pathology")
DISEASE_PATTERN_LIST.append("pathologies")
DISEASE_PATTERN_LIST.append("sign")
DISEASE_PATTERN_LIST.append("injury")
DISEASE_PATTERN_LIST.append("injuries")
#DISEASE_PATTERN_LIST.append("finding")
DISEASE_PATTERN_LIST.append("dystrophy")
DISEASE_PATTERN_LIST.append("dystrophie")
DISEASE_PATTERN_LIST.append("dystrophic")
DISEASE_PATTERN_LIST.append("lesion")
DISEASE_PATTERN_LIST.append("carcinoma")
DISEASE_PATTERN_LIST.append("cancer")
DISEASE_PATTERN_LIST.append("tumor")
DISEASE_PATTERN_LIST.append("tumour")
# aBravo 23.12.2014
DISEASE_PATTERN_LIST.append("patient")
# aBravo 12.06.2015
DISEASE_PATTERN_LIST.append("virus")
DISEASE_PATTERN_LIST.append("treat")

DISEASE_PATTERN = r'[\s]{,2}('+'|'.join(sorted(DISEASE_PATTERN_LIST))+')[s]?'
DISEASE_PATTERN_REGEX = regex.compile(DISEASE_PATTERN)

DISEASE_ACRONYM_PATTERN = regex.compile(r'(' + DISEASE_PATTERN + r')?\s?' + r'\(([a-zA-Z0-9\-]+)\)')


DNA_PATTER_LIST = []
DNA_PATTER_LIST.append("dna")
DNA_PATTER_LIST.append("DNA")
DNA_PATTERN = regex.compile(r'[\s]{,2}('+'|'.join(sorted(DNA_PATTER_LIST))+')[s]?')
    
CELL_LINE_PATTERN_LIST = []
CELL_LINE_PATTERN_LIST.append("cell")
CELL_LINE_PATTERN_LIST.append("line")
CELL_LINE_PATTERN = regex.compile(r'[\s]{,2}('+'|'.join(sorted(CELL_LINE_PATTERN_LIST))+')[s]?')

#GENE_DATABASE = "gene2015"
#XREF_GENE_COLL = "cross_references_NUH_0221"
#GENE_DICT_COLL = "gene_dict_0221"

#GENE_HIST_COLL = "gene_historic_0221"
