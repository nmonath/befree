# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import re,regex, pickle, sys
from pkg_resources import resource_filename
#current_path = sys.path[0]
#mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
#sys.path.append(mongodb_path)
# mongodb_path = "/".join(current_path.split("/")[:-1]) + "/dictionaries"
# sys.path.append(mongodb_path)
from ..dictionaries.dict_constants import GENE_DB_NAME, CROSS_REF, VER, ENTREZ_GENE, XREF_FIELD,\
    ID, DICT_SYMBOL
from ..mongodb.MongoConnection import MongoConnection
from .BeFree_constants import RESULT_SENT_NUM, RESULT_ENTITY_OFFSET

real_number = r"[-+]?(\d*\.\d+|\d+)"

def filt_score(ini, end, sent):
    
    bef_list = sent[:ini].split(" ")
    aft_list =sent[end:].split(" ")
    filter_score = ["scale", "score", "rating", "rate", "ratio"]
    
    if len(bef_list)>0 and bef_list[-1] in filter_score:
        return True
    
    if len(bef_list)>1 and  bef_list[-2] in filter_score:
        return True
    
    if len(aft_list)>0 and aft_list[0] in filter_score:
        return True
        
    if len(aft_list)>1 and aft_list[1] in filter_score:
        return True
    
    return False

def get_xref2entrez(use_pickle=False):
    if use_pickle:
        xref_dict = pickle.load(open(resource_filename('befree.in', 'xref2entrez.pkl'), 'rb'))
        return xref_dict
    xref_conn = MongoConnection(GENE_DB_NAME, CROSS_REF + VER)
    res = xref_conn.find()
    xref_dict = {}
    for rec in res:
        gene_id_list = rec.get(ENTREZ_GENE + ID)
        if gene_id_list:
            xref = rec.get(XREF_FIELD)
            xref_dict[xref] = gene_id_list
     
    return xref_dict

def get_gene_symbol_dict(use_pickle=False):
    if use_pickle:
        gene_dict = pickle.load(open(resource_filename('befree.in', 'gene_symbol.pkl'), 'rb'))
        return gene_dict
    gene_conn = MongoConnection(GENE_DB_NAME, ENTREZ_GENE+VER)
    records = gene_conn.find({})
    gene_dict = {}
    for rec in records:
        gene_id = rec[ENTREZ_GENE + ID]
        gene_symbol = rec[DICT_SYMBOL]
        gene_dict[gene_symbol] = gene_id
    return gene_dict

def get_results_pattern():
    return regex.compile(r"[a-zA-Z0-9]+\s?[=]\s?[%]?" + real_number)

def get_units_pattern():
    units_list = []
    units_list.append(u"mg")
    units_list.append(u"kg")
    units_list.append(u"ml")
    units_list.append(u"μmol/L")
    units_list.append(u"μmol")
    units_list.append(u"mg/L")
    units_list.append(u"kb")
    units_list = sorted(units_list, reverse=True)
    return  regex.compile(real_number + r"\s?" + "(" + "|".join(units_list) + ")")

def get_percent_pattern():
    return regex.compile(real_number + r"\s?[%]")
    
def get_real_number_pattern():
    real_number = r"[-+]?\d*\.\d+"
    return regex.compile(real_number)

def add_elem_dictionary(dictionary, key, elem, repeat = False):
    
    if not repeat:
        aux = dictionary.get(key, {})
        aux[elem] = 1
        dictionary[key] = aux
        return dictionary
    
    aux = dictionary.get(key, [])
    aux.append(elem)
    dictionary[key] = aux
    return dictionary

def add_one_dictionary(dictionary, key):
    aux = dictionary.get(key, 0)
    dictionary[key] = aux + 1
    return dictionary


def get_greek_letters_lw():
    greek_symbols_lw = {}
    greek_symbols_lw[u"α"] = "alpha"
    greek_symbols_lw[u"β"] = "beta"
    greek_symbols_lw[u"γ"] = "gamma"
    greek_symbols_lw[u"δ"] = "delta"
    greek_symbols_lw[u"ε"] = "epsilon"
    greek_symbols_lw[u"ζ"] = "zeta"
    greek_symbols_lw[u"η"] = "eta"
    greek_symbols_lw[u"θ"] = "theta"
    greek_symbols_lw[u"ι"] = "iota"
    greek_symbols_lw[u"κ"] = "kappa"
    greek_symbols_lw[u"λ"] = "lambda"
    greek_symbols_lw[u"μ"] = "mu"
    greek_symbols_lw[u"ν"] = "nu"
    greek_symbols_lw[u"ξ"] = "xi"
    greek_symbols_lw[u"π"] = "pi"
    greek_symbols_lw[u"ρ"] = "rho"
    greek_symbols_lw[u"σ"] = "sigma"
    greek_symbols_lw[u"τ"] = "tau"
    greek_symbols_lw[u"υ"] = "upsilon"
    greek_symbols_lw[u"φ"] = "phi"
    greek_symbols_lw[u"χ"] = "chi"
    greek_symbols_lw[u"ψ"] = "psi"
    greek_symbols_lw[u"ω"] = "omega"
    return greek_symbols_lw

def get_normalization_term(text):
    text = regex.sub(r'[àâäáÀÂÄÁ]', 'a', text)
    text = regex.sub(r'[éèëêÈËÊÉ]', 'e', text)
    text = regex.sub(r'[ïíìîÏÎÍÌ]', 'i', text)
    text = regex.sub(r'[öóòôÖÔÓÒ]', 'o', text)
    text = regex.sub(r'[ûüùúÜÛÙÚ]', 'u', text)
    text = regex.sub(r'[çÇ]', 'c', text)
    text = regex.sub(r'[ñÑ]', 'n', text)
    text = text.replace("'s", "  ")
    text = text.replace("`s", "  ")
    
    greek = get_greek_letters_lw()
    matches = regex.finditer(r'[^a-zA-Z0-9\s#'+''.join(greek.keys())+']{1}', text)
    for m in matches:
        ini_part = text[:m.start()]
        end_part = text[m.end():]
        text = ini_part + " " + end_part
    
    # PLURAL MANAGEMENT
    acr_plural_pat = regex.compile(r'\s[A-Z0-9\-]+([s])\s')
    matches = acr_plural_pat.finditer(text)
    for match in matches:
        ini= match.end()-2
        end= match.end()-1
        text = text[:ini] + " " + text[end:]
    
    text = text.lower()
    
    return text

def get_xml_symbol(tag):
    tag_dict = {}
    tag_dict["&quot;"] = '"' 
    tag_dict["&amp;"] = "&" 
    tag_dict["&apos;"] = "'" 
    tag_dict["&lt;"] = "<" 
    tag_dict["&gt;"] = ">" 
    tag_dict["&mgr;"] = "mu"
    tag_dict["&hyp;"] = "'"
    tag_dict["&mdash;"] = "/"
    return tag_dict.get(tag, "#")

def replace_xml_tags_filtering(text):
    pattern = regex.compile(r"&[a-z]+;")
    matches = pattern.finditer(text)
    for m in matches:
        text = text.replace(m.group(0), get_xml_symbol(m.group(0)))
    #text = unescape(text)
    return text

def remove_extra_spaces(text, max_space = 3):
    p = regex.compile(r'\s+')
    matches = p.finditer(text)
    count = 0
    for m in matches:
        ini = m.start()
        end = m.end()
        if max_space < end-ini:
            continue
        ini_part = text[:ini-count]
        end_part = text[end-count:]
        text = ini_part +" "+ end_part
        count+= end-ini-1
    return text

def term_curation(term):
    term = get_normalization_term(term)
    term = replace_xml_tags_filtering(term)
    term = remove_extra_spaces(term)
    return term.strip()

def xml_tags_filtering(text):
    return regex.sub(r"&[a-z]+;", "#", text)

def percent_filtering(text, PERCENT_PATTERN):
    matches = PERCENT_PATTERN.finditer(text)#, pos=None, endpos=None, overlapped=True, concurrent=True)
    for m in matches:
        ini = m.start()
        end = m.end()
        before = text[:ini] 
        after = text[end:]
        num = end-ini
        aux = "".join(['#']*num)    
        text = before + aux + after
    return text

def results_filtering(text, RESULTS_PATTERN):
    matches = RESULTS_PATTERN.finditer(text)#, pos=None, endpos=None, overlapped=True, concurrent=True)
    for m in matches:
        ini = m.start()
        end = m.end()
        before = text[:ini] 
        after = text[end:]
        num = end-ini
        aux = "".join(['#']*num)    
        text = before + aux + after
    return text

def units_of_measurement_filtering(text, UNITS_PATTERN):
    
    matches = UNITS_PATTERN.finditer(text)#, pos=None, endpos=None, overlapped=True, concurrent=True)
    
    for m in matches:
        ini = m.start()
        end = m.end()
        before = text[:ini] 
        after = text[end:]
        num = end-ini
        aux = "".join(['#']*num)   
        text = before + aux + after
    return text.replace("%", "#")

def real_numbers_filtering(text, REAL_NUMBER_PATTERN):
    matches = REAL_NUMBER_PATTERN.finditer(text)
    
    for m in matches:
        ini = m.start()
        end = m.end()
        before = text[:ini] 
        after = text[end:]
        num = end-ini
        aux = "".join(['#']*num) 
        text = before + aux + after
    
    return text

def enumerations_detection(sentence):
    i=1
    match = regex.search(str(i)+r'\)', sentence)
    list_aux = []
    end = 0
    while match:
        match.start()
        ini = match.start() +end
        end = match.end() +end
        list_aux.append(str(ini)+"-"+str(end))
        i+=1
        match = regex.search(str(i)+r'\)', sentence[end:])
        
    if not len(list_aux):
        roman_list = ["i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv", "xvi"]
        i=0
        match = regex.search(roman_list[i]+r'\)', sentence)
        list_aux = []
        end = 0
        while match:
            match.start()
            ini = match.start() +end
            end = match.end() +end
            list_aux.append(str(ini)+"-"+str(end))
            i+=1
            match = regex.search(roman_list[i]+r'\)', sentence[end:])
       
    if len(list_aux) > 2:
        for offset in list_aux:
            ini = int(offset.split("-")[0])
            end = int(offset.split("-")[1])
            size = end-ini
            sep = "".join(["#"]*size)
            sentence = sentence[:ini] + sep + sentence[end:]
        #print sentence
    
    return sentence

def special_characters(sentence):
    sentence = sentence.replace("=", "#")
    sentence = sentence.replace(">", "#")
    sentence = sentence.replace("<", "#")
    sentence = sentence.replace("~", "#")
    sentence = sentence.replace("i.e.", "####")
    return sentence

def get_offset_correction(offset_corr, ini, end):
    #end -= 1
    ini = offset_correction(offset_corr, ini)
    end = offset_correction(offset_corr, end)
    return ini, end

def offset_correction(offset_corr, num):
    aux = num
    lst = sorted(offset_corr.keys())
    if len(lst):
        i = 0
        while aux > lst[i]:
            aux += offset_corr[lst[i]]
            i += 1
            if i == len(lst):
                break
    return aux

def is_overlap(range1, range2):       
    ini1 = int(range1[0])
    end1 = int(range1[1])
    ini2 = int(range2[0])
    end2 = int(range2[1])
    result = True 
    if (ini2 < ini1 and end2 < ini1) or (ini2 > end1 and end2 > end1):
        result = False
    return result


def get_extra_spaces_position(text, max_space = 3):
    matches = regex.finditer(r'[\s]{2,}', text)
    offset_corr = {}
    for m in matches:
        ini = m.start()
        end = m.end()
        if max_space < end-ini:
            continue
        offset_corr[ini] = end - ini -1
    return offset_corr

def split_sentences(text, tokenizer, withTitle = True):
    
    text = text.replace(").(", ")#(").replace(").[", ")#[").replace("al.(", "al#(").replace("?'", "'?").replace('?"', '"?')
    
    match_no_sep = [")", ";", "]", ":"]
    match_no_sep = [")", ";", "]"]
    sentences_pre = tokenizer.tokenize(text)
    
    #aBravo 11.06.2015
    sentences_aux = []
    title = 0
    if withTitle:
        title = 1
    size_pre = 0
    for sent in sentences_pre:
        if not title:
            if len(sentences_aux):
                match = re.match(r'[a-z\);]', sent)   
                if match:
                    sent_aux = sentences_aux.pop()
                    sep = " "
                    if match.group(0) in match_no_sep :
                        sep = ""
                    sent = sent_aux + sep + sent
                elif len(sent) < 40:
                    sent_aux = sentences_aux.pop()
                    sent = sent_aux + sent
                elif size_pre < 40:
                    sent_aux = sentences_aux.pop()
                    sent = sent_aux + sent
                    
                
        title = 0
        size_pre = len(sent)
        sent = sent.replace(")#(", ").(").replace(")#[", ").[").replace("al#(","al.(").replace("'?", "?'").replace('"?','?"')
        sentences_aux.append(sent)

    return sentences_aux

    
    
def get_entity_information(lines):
    entity_sent = {}
    entity_offset_lin = {}
    for lin_split in lines:
        sent = lin_split[RESULT_SENT_NUM]
        offset = lin_split[RESULT_ENTITY_OFFSET]
        entity_offset_lin[sent+ "-" +offset] = lin_split
        entity_sent = add_elem_dictionary(entity_sent, sent, offset)
    return entity_sent, entity_offset_lin


def overlap(range1, range2):
    ini1 = int(range1[0])
    end1 = int(range1[1])
    ini2 = int(range2[0])
    end2 = int(range2[1])

    inis = ini1 - ini2
    ends = end1 - end2
    
    if inis > 0:
        if ends > 0:
            return -1 # range1, range2
        elif ends <= 0:
            return 1
    elif inis < 0:
        if ends >= 0:
            return 0
        elif ends < 0:
            return -1 # range1, range2
    else:
        if ends >= 0:
            return 0
        elif ends < 0:
            return 1


def get_mesh_disease_dictionary(mesh_file):
    mesh_dict = {}
    no_valid_classes = ["C22", "C25", "C26", "F04"]
    for lin in open(mesh_file):
        lin_split = lin.strip().split(";")
        term = lin_split[0]
        node = lin_split[1]
        clas = node.split(".")[0]
        if "C" in clas or "F" in clas:
            if not clas in no_valid_classes:
                mesh_dict = add_elem_dictionary(mesh_dict, term, node)
    return mesh_dict


def get_befree_logo():
    
    return """                       s-                                                        
                     --o:                         ```                       
                    -oy:.                         `s/::..`                  
                  +dMMMN.                         `+//o/++/`                
               `+yy+hhhNd                          o////+++:-`              
            ./hdo/`o-o`sd-         /+//+o+:---.`   `o////ooo--              
        -:/s////-oho/d/s+m`         `:s++so:+/+:o/--+sooyo:+o+              
     .:://--+:  ++   s.-dyh.          -/o:/+//:+-:++s//+::+//               
   `+::+``o:  `s:    s` /:+o-           ..-+s/:/+s+o+++/oo/:/               
  `o`+: `s`  `y-     h`  y o//              `.-///+:+oo+/+o:/`              
  s+sy/:y`   y:      d   o: s:/                `/ooooo++/+/oy/`.:o/:.-.     
 -:`o  s+///sd-`   `/ms  y/ +:o.                 :/+ooy+/-/o++oy+oss+///:.` 
 o`/- `y   `d`.::ohhMyh -M- /+./                  //+o//+:o//o+so/:/.`      
 /.+- ::   ++    ``yh  hmm++y:-:              ++ `/++++:/+o+/so+/`          
 `o.o ::   h`     oNs   h. -s-d`             y++`/s///+//+++:```            
  `++..o  `y     :No   ho  h+s-             N+-/y+++/o::+:.`                
   ./o.s  .o    .N:    yh oys+`             oho+o+s:-.`                     
    `+s/+ ./   `dy    `mo.N+-              `//`--`                          
    /hNmm+/o` -sM.  `/ms//ss             .`                               
    :oydNMMMMNNmMddddh/-`                                                   
         `-/oymNMMMy                                                         
                .:++                                                                                     
        вισ-єитιту fιи∂єя αи∂ яєℓαтισи єχтяα¢тισи
        http://ibi.imim.es/befree
"""



def get_ner_process():
    return"""▒█▄░▒█ ▒█▀▀▀ ▒█▀▀█ 
▒█▒█▒█ ▒█▀▀▀ ▒█▄▄▀ 
▒█░░▀█ ▒█▄▄▄ ▒█░▒█ """


def get_re_process():
    return"""▒█▀▀█ ▒█▀▀▀ 
▒█▄▄▀ ▒█▀▀▀ 
▒█░▒█ ▒█▄▄▄ """

def get_results_screen():
    res="""###############################
#         NER RESULTS         #
###############################"""
    return """..::RESULTS::.."""
def get_results_screen_old():
    return"""▒█▀▀█ ▒█▀▀▀ ▒█▀▀▀█ ▒█░▒█ ▒█░░░ ▀▀█▀▀ ▒█▀▀▀█ ▄ 
▒█▄▄▀ ▒█▀▀▀ ░▀▀▀▄▄ ▒█░▒█ ▒█░░░ ░▒█░░ ░▀▀▀▄▄ ░ 
▒█░▒█ ▒█▄▄▄ ▒█▄▄▄█ ░▀▄▄▀ ▒█▄▄█ ░▒█░░ ▒█▄▄▄█ ▀ 
"""


def get_ner_process_old():
    return"""▒█▄░▒█ ▒█▀▀▀ ▒█▀▀█ 
▒█▒█▒█ ▒█▀▀▀ ▒█▄▄▀ 
▒█░░▀█ ▒█▄▄▄ ▒█░▒█ 

▒█▀▀█ ▒█▀▀█ ▒█▀▀▀█ ▒█▀▀█ ▒█▀▀▀ ▒█▀▀▀█ ▒█▀▀▀█ 
▒█▄▄█ ▒█▄▄▀ ▒█░░▒█ ▒█░░░ ▒█▀▀▀ ░▀▀▀▄▄ ░▀▀▀▄▄ 
▒█░░░ ▒█░▒█ ▒█▄▄▄█ ▒█▄▄█ ▒█▄▄▄ ▒█▄▄▄█ ▒█▄▄▄█ """


def get_re_process_old():
    return"""▒█▀▀█ ▒█▀▀▀ 
▒█▄▄▀ ▒█▀▀▀ 
▒█░▒█ ▒█▄▄▄ 

▒█▀▀█ ▒█▀▀█ ▒█▀▀▀█ ▒█▀▀█ ▒█▀▀▀ ▒█▀▀▀█ ▒█▀▀▀█ 
▒█▄▄█ ▒█▄▄▀ ▒█░░▒█ ▒█░░░ ▒█▀▀▀ ░▀▀▀▄▄ ░▀▀▀▄▄ 
▒█░░░ ▒█░▒█ ▒█▄▄▄█ ▒█▄▄█ ▒█▄▄▄ ▒█▄▄▄█ ▒█▄▄▄█ """



if __name__ == '__main__':
    sentence = "Among the dizygotic twin pairs discordant for MDD, the CFR was .63% lower .321 in the twins with MDD than in their brothers without MDD (2.36 vs 2.74) (EC23 = .03)."
    
    sentence = "The next best SNP was rs12201676 located at 6q15 (p=2.67 × 10(-4), 2.12 × 10(-5), 3.88 × 10(-8) for schizophrenia, bipolar disorder and meta-analysis, respectively), near two flanking genes, GABRR1 and GABRR2 (15 and 17kb away, respectively)."
    
    
    
    sentence = "the experimental autoimmune/allergic encephalomyelitis (1) the experimental autoimmune/allergic encephalomyelitis (EAE); (2) the virally-induced chronic emyelinating disease, known as Theile murine encephalomyelitis virus (TMEV) infection and (3) the toxin-induced demyelination."
    
    #sentence = "the experimental autoimmune/allergic encephalomyelitis i) the experimental autoimmune/allergic encephalomyelitis (EAE); ii) the virally-induced chronic emyelinating disease, known as Theile murine encephalomyelitis virus (TMEV) infection and iii) the toxin-induced demyelination."
    
    
    print(enumerations_detection(sentence))
    sys.exit()
    matches = regex.finditer(r'\([0-9ivx]\)', sentence)
    for match in matches:
        print(match.group(0))
        #print match.start()
        #print match.end()
        
    
    i=1
    match = regex.search(str(i)+r'\)', sentence)
    list_aux = []
    end = 0
    print(sentence)
    while match:
        print(match.group(0))
        match.start()
        ini = match.start() +end
        end = match.end() +end
        list_aux.append(str(ini)+"-"+str(end))
        i+=1
        match = regex.search(str(i)+r'\)', sentence[end:])
       
    if len(list_aux) > 2:
        for offset in list_aux:
            ini = int(offset.split("-")[0])
            end = int(offset.split("-")[1])
            
            size = end-ini
            sep = "".join(["#"]*size)
            
            sentence = sentence[:ini] + sep + sentence[end:]
    
    print(sentence)
    sys.exit()
    
    
    sentence = "Interaction of Hb Grey Lynn (Vientiane) [α91(FG3)Leu&gt;Phe (α1)] with Hb E [β26(B8) Glu&gt;Lys] and α(+)-thalassemia: Molecular and Hematological Analysis."
    
    
    print(replace_xml_tags_filtering(sentence))
    
    sys.exit()
    
    #sentence = "Histamine levels are higher   in alcohol-preferring than in alcohol-nonpreferring rat brains, and expression of histamine H(3)-receptor (H (3)-R)  is different in key areas for addictive behavior."
    #sentence = "The best associated SNP rs11789399 was located at 9q33.1 (p=2.38 × 10(-6), 5.74 × 10(-4), and 5.56 × 10(-9), for schizophrenia, bipolar disorder and meta-analysis of schizophrenia and bipolar disorder, respectively), where one flanking gene, ASTN2 (220kb away) has been associated with attention deficit/hyperactivity disorder and schizophrenia."
    
    
    pat = get_real_number_pattern()
    
    sentence = sentence.decode('utf-8')
    
    matches = pat.finditer(sentence)
    for m in matches:
        print(m.group(0))
    
    sentence = results_filtering(sentence, get_results_pattern())
    print(sentence)
    
    
    sentence = percent_filtering(sentence, get_percent_pattern())
    print(sentence)
    
    sentence = real_numbers_filtering(sentence, get_real_number_pattern())
    print(sentence)
    sentence = special_characters(sentence)
    print(sentence)
    print(get_normalization_term(sentence))
    
    sentence = "Histamine levels are higher   in alcohol-preferring than in alcohol-nonpreferring rat brains, and expression of histamine H(3)-receptor (H (3)-R)  is different in key areas for addictive behavior."
    sentence = "The best associated SNP rs11789399 was located at 9q33.1 (p=2.38 × 10(-6), 5.74 × 10(-4), and 5.56 × 10(-9), for schizophrenia, bipolar disorder and meta-analysis of schizophrenia and bipolar disorder, respectively), where one flanking gene, ASTN2 (220kb away) has been associated with attention deficit/hyperactivity disorder and schizophrenia."
    
    sentence = special_characters(sentence)
    #print sentence
    #print get_normalization_term(sentence)

