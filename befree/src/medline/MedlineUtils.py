# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import re, nltk, sys
from datetime import datetime
from .medline_constants import DATE_CREATED_TAG, YEAR_TAG, MONTH_TAG, DAY_TAG,\
    DESCRIPTOR_TAG, QUALIFIER_TAG, MESH_HEADING_TAG, SPLIT_CHAR, PMID_SPLIT,\
    TAG_SPLIT, NO_MESH_HEADING_LIST, ID_FIELD, TITLE_FIELD, XML_FIELD,\
    LABEL_LIST_FIELD, ABSTRACT_LIST_FIELD, MESH_FIELD,\
    KEYWORDS_FIELD, YEAR_FIELD, ISSN_FIELD, ABSTRACT_FIELD, DELETION_SPLIT,\
    JOURNAL_TITLE_FIELD, JOURNAL_ABBRE_FIELD, MESH_DESC_FIELD,\
    MESH_QUAL_FIELD, MESH_IDS_FIELD, ABSTRACT_LIST_BY_SENT_FIELD
#current_path = sys.path[0]
#mongodb_path = "/".join(current_path.split("/")[:-1]) + "/ner"
#sys.path.append(mongodb_path)
from ..ner.BeFree_utils import replace_xml_tags_filtering

def get_pmidFromXML(citation):
    pmid = citation.split(PMID_SPLIT)[0].split(">")[-1]
    return pmid   

def medlineCitationSplit(xmlText):
    abstracts = xmlText.split(TAG_SPLIT)
    abstracts[-1] = abstracts[-1].replace("</MedlineCitationSet>","")
    for i in range(0, len(abstracts)):
        abstracts[i] = TAG_SPLIT + abstracts[i]
    return abstracts[1:]

def get_title(xmlText):
    if "<ArticleTitle/>" in xmlText:
        return ""
    title = xmlText.split("<ArticleTitle>")[1].split("</ArticleTitle>")[0]
    return title



def get_year(xmlText):
    if xmlText.find("<DateCompleted>") > -1:
        date = xmlText.split("<DateCompleted>")[1].split("</DateCompleted>")[0]
        return date.strip().split("<Year>")[1].split("</Year>")[0]
    elif xmlText.find("<DateCreated>") > -1:
        date = xmlText.split("<DateCreated>")[1].split("</DateCreated>")[0]
        return date.strip().split("<Year>")[1].split("</Year>")[0]
    else:
        return None
    
def get_journal_title(xmlText):
    
    xmlText = xmlText.split("<Journal>")[1].split("</Journal>")[0]
    
    if "<Title>" in xmlText:
        return xmlText.split("<Title>")[1].split("</Title>")[0]
    
    return None

def get_journal_abbre(xmlText):
    xmlText = xmlText.split("<Journal>")[1].split("</Journal>")[0]
    
    if "<ISOAbbreviation>" in xmlText:
        return xmlText.split("<ISOAbbreviation>")[1].split("</ISOAbbreviation>")[0]
    
    return None

def get_issn(xmlText):
    """
    xmlText = xmlText.split("<ISSN Issn")[1]
    xmlText = xmlText[xmlText.find(">")+1:]
    return xmlText.split("</ISSN>")[0]
    """
    
    if xmlText.find("<ISSN Issn")>-1:
        xmlText = xmlText.split("<ISSN Issn")[1]
        xmlText = xmlText[xmlText.find(">")+1:]
        return xmlText.split("</ISSN>")[0]
    elif xmlText.find("<ISSNLinking")>-1:
        return xmlText.split("<ISSNLinking>")[1].split("</ISSNLinking>")[0]
    else:
        return None

def get_document(xmlText, tokenizer):
    
    xmlText = xmlText.replace("<PubDate><ISOAbbreviation>", "<PubDate>")
    pmid = get_pmidFromXML(xmlText) 
    title, all_abstract, label_abstract, abstract_text_list, abstract_text_list_by_sentence = get_abstract_parts(xmlText, tokenizer)
    year = get_year(xmlText)
    journal_title = get_journal_title(xmlText)
    journal_abbre = get_journal_abbre(xmlText)
    issn = get_issn(xmlText)
    desc_dict, qual_dict = get_mesh_descriptors_and_qualifiers(xmlText)
    mesh_list_with_mesh_id = get_mesh_list_with_mesh_id(xmlText)
    mesh = get_mesh_terms(xmlText)
    kwords = get_keywords(xmlText)
    newDoc = {}
    
    # NOT NULLs
    newDoc["pmid"] = pmid
    newDoc[ID_FIELD] = pmid
    newDoc[TITLE_FIELD] = title
    newDoc[XML_FIELD] = xmlText
    newDoc[LABEL_LIST_FIELD] = label_abstract
    newDoc[ABSTRACT_LIST_FIELD] = abstract_text_list
    
    newDoc[ABSTRACT_LIST_BY_SENT_FIELD] = abstract_text_list_by_sentence
    
    # NULLs
    if mesh:
        newDoc[MESH_FIELD] = mesh
    if kwords:
        newDoc[KEYWORDS_FIELD] = kwords
    if year:
        newDoc[YEAR_FIELD] = year
    if issn:
        newDoc[ISSN_FIELD] = issn
    if len(all_abstract):
        newDoc[ABSTRACT_FIELD] = all_abstract
    if journal_title:    
        newDoc[JOURNAL_TITLE_FIELD] = journal_title
    if journal_abbre:
        newDoc[JOURNAL_ABBRE_FIELD] = journal_abbre
    if len(desc_dict):
        newDoc[MESH_DESC_FIELD] = desc_dict
    if len(qual_dict):
        newDoc[MESH_QUAL_FIELD] = qual_dict
    if mesh_list_with_mesh_id:
        newDoc[MESH_IDS_FIELD] = mesh_list_with_mesh_id
    return newDoc

def get_keywords(xml_text):
    kw_list = {}
    
    if xml_text.find("<KeywordList") != -1:
        ini = xml_text.index("<KeywordList")
        end = xml_text.index("</KeywordList")
        kw_text = xml_text[ini:end]
        kw_text_split = kw_text.split("</Keyword>")
        for mts in kw_text_split:
            if mts.find("<Keyword ") != -1:
                ini_aux = mts.index("<Keyword ")
                ini = mts[ini_aux:].index(">")
                kw = mts[ini_aux:][ini+1:]
                
                kw_list[kw] = 1
        return list(kw_list.keys())
    
    return None

def add_elem_dictionary(dictionary, key, elem, repet = False):
    if key in dictionary:
        aux = dictionary.get(key)
        if repet:
            aux.append(elem)
            dictionary[key] = aux
        else:    
            if not elem in aux:
                aux.append(elem)
                dictionary[key] = aux
    else:
        dictionary[key] = [elem]
    return dictionary


def mesh_curation(mesh):
    return mesh.replace('&amp;', "&").replace('.', " ").replace('(', " ").replace(')', " ")

def get_mesh_descriptors_and_qualifiers(xml_text):
    desc_pat = re.compile(r'([^>]+)</DescriptorName>')
    qual_pat = re.compile(r'([^>]+)</QualifierName>')
    desc_dict = {}
    qual_dict = {}
    if xml_text.find("<MeshHeadingList>") != -1:
        ini = xml_text.index("<MeshHeadingList>")
        end = xml_text.index("</MeshHeadingList>")
        
        xml_text = xml_text[ini:end]
        mesh_text_split = xml_text.split("</MeshHeading>\n")
        
        
        for mts in mesh_text_split:
            
            desc_name = ""
            qual_name = ""
            for lin in mts.split("\n"):
                
                desc_match = desc_pat.search(lin)
                if desc_match:
                    desc_name = mesh_curation(desc_match.group(1))
                
                qual_match = qual_pat.search(lin)
                if qual_match:
                    qual_name = mesh_curation(qual_match.group(1))
                    add_elem_dictionary(qual_dict, qual_name, desc_name)
                    add_elem_dictionary(desc_dict, desc_name, qual_name)
        
        
    return desc_dict, qual_dict

def get_mesh_list_with_mesh_id(xml_text):
    mesh_list = {}
    desc_pat = re.compile(r'([^>]+)</DescriptorName>')
    iden_pat = re.compile(r'UI=\"(D[\d]+)\">')
    
    if xml_text.find("<MeshHeadingList>") != -1:
        ini = xml_text.index("<MeshHeadingList>")
        end = xml_text.index("</MeshHeadingList>")
        
        xml_text = xml_text[ini:end]
        mesh_text_split = xml_text.split("</MeshHeading>\n")
        
        for mts in mesh_text_split:
            for lin in mts.split("\n"):
                desc_match = desc_pat.search(lin)
                
                if desc_match:
                    iden = "-1"
                    iden_match = iden_pat.search(lin)
                    if iden_match:
                        iden = iden_match.group(1)
                    mesh_list[mesh_curation(desc_match.group(1))] = iden
                    
        return mesh_list
    return None

def get_mesh_terms(xml_text):
    mesh_list = {}
    desc_pat = re.compile(r'([^>]+)</DescriptorName>')
    
    if xml_text.find("<MeshHeadingList>") != -1:
        ini = xml_text.index("<MeshHeadingList>")
        end = xml_text.index("</MeshHeadingList>")
        
        xml_text = xml_text[ini:end]
        mesh_text_split = xml_text.split("</MeshHeading>\n")
        
        for mts in mesh_text_split:
            for lin in mts.split("\n"):
                desc_match = desc_pat.search(lin)
                if desc_match:
                    mesh_list[mesh_curation(desc_match.group(1))] = 1
        return list(mesh_list.keys())
    return None



def split_sentences(text, tokenizer, withTitle = True):
    
    text = text.replace(").(", ")#(").replace(").[", ")#[").replace("al.(", "al#(").replace("?'", "'?").replace('?"', '"?')
    
    match_no_sep = [")", ";", "]", ":"]
    match_no_sep = [")", ";", "]"]
    sentences_pre = tokenizer.tokenize(text)
    
    #aBravo 11.06.2015
    sentences_aux = []
    title = 0
    if withTitle:
        title = 1
    size_pre = 0
    for sent in sentences_pre:
        if not title:
            if len(sentences_aux):
                match = re.match(r'[a-z\);]', sent)   
                if match:
                    sent_aux = sentences_aux.pop()
                    sep = " "
                    if match.group(0) in match_no_sep :
                        sep = ""
                    sent = sent_aux + sep + sent
                elif len(sent) < 40:
                    sent_aux = sentences_aux.pop()
                    sent = sent_aux + sent
                elif size_pre < 40:
                    sent_aux = sentences_aux.pop()
                    sent = sent_aux + sent
                    
                
        title = 0
        size_pre = len(sent)
        sent = sent.replace(")#(", ").(").replace(")#[", ").[").replace("al#(","al.(").replace("'?", "?'").replace('"?','?"')
        sent=sent.strip()
        sentences_aux.append(sent)
    return sentences_aux


def get_abstract_parts(xml_text, tokenizer):
    title = replace_xml_tags_filtering(get_title(xml_text))
    label_abstract = {}
    abstract_text_list = {}
    # aBravo Add 06.02.17
    abstract_text_list_by_sentence = {}
    abstract_text_list_by_sentence[u"TITLE"] = title
    
    label_abstract[u"TITLE"] = 0
    abstract_text_list[u"TITLE"] = title
    all_abstract = ""
    if xml_text.find("<Abstract>") != -1:
        ini = xml_text.index("<Abstract>")
        
        end = xml_text.index("</Abstract>")
        if xml_text.find("<CopyrightInformation>") != -1:
            end = xml_text.index("<CopyrightInformation>")
        
        abstract_text= xml_text[ini+len("<Abstract>"):end].strip().split("<AbstractText Label=")
        if len(abstract_text) ==1:
            text=  abstract_text[0].replace("</AbstractText>","").replace("<AbstractText>","")
            label_abstract[u"ALL_TEXT"] = 1
            
            # aBravo Add 06.02.17
            text = text.split("</Abstract>")[0].replace("<AbstractText NlmCategory=\"UNASSIGNED\">", "")
            text = replace_xml_tags_filtering(text)
            abstract_text_list_by_sentence[u"ALL_TEXT"] = split_sentences(text, tokenizer)
            # aBravo
            
            abstract_text_list[u"ALL_TEXT"] = text
            all_abstract = text
            
        else:
            abstract_text = abstract_text[1:]
            num = 0
            for part in abstract_text:
                part=  part.strip()
                ind = part.find(">") + 1
                parr = part[ind:].replace("</AbstractText>","")
                label = part[:ind]
                if len(parr):
                    num+=1
                    label = label[1:label[1:].index('"')+1]
                    i=1
                    copy_label = label
                    while label in label_abstract:
                        i+=1
                        label = copy_label + "_"+ str(i)
                    label_abstract[label] = num
                    
                    # aBravo Add 06.02.17
                    parr = parr.split("</Abstract>")[0].replace("<AbstractText NlmCategory=\"UNASSIGNED\">", "")
                    parr = replace_xml_tags_filtering(parr)
                    abstract_text_list_by_sentence[label] = split_sentences(parr, tokenizer) 
                    # aBravo
                    
                    abstract_text_list[label] = parr
                    all_abstract = all_abstract + " " + parr
    return title, all_abstract, label_abstract, abstract_text_list, abstract_text_list_by_sentence
    

def get_abstract(xmlText):
    absText = []
    text = ""
    if xmlText.find("<Abstract>") != -1:
        ini = xmlText.index("<Abstract>")
        end = xmlText.index("</Abstract>")
        abstracts = xmlText[ini+len("<Abstract>"):end].strip().split("</AbstractText>")
        for i in range(0, len(abstracts)-1):
            if len(abstracts[i]) > 3:
                ind = abstracts[i].index(">")
                absText.append(abstracts[i][ind+1:])            
        text = " ".join(absText)
    return text

def get_abstract_full_format(xmlText):
    absText = []
    text = ""
    xml_tag = re.compile(r'(&[az]+;)')
    if xmlText.find("<Abstract>") != -1:
        ini = xmlText.index("<Abstract>")
        end = xmlText.index("</Abstract>")
        abstracts = xmlText[ini+len("<Abstract>"):end].strip().split("</AbstractText>")
        for i in range(0, len(abstracts)-1):
            result = re.findall(r'AbstractText Label="([A-Z\s/]+)"', abstracts[i])
            if len(abstracts[i]) > 3:
                ind = abstracts[i].index(">")
                abstr = abstracts[i][ind+1:]
                if len(result) and result[0] != "UNLABELLED":
                    abstr = result[0]+ ": " + abstr
                
                result = re.findall(r'(&[a-z]+;)', abstr)
                if len(result):
                    abstr = abstr.replace("&quot;", '"')
                    abstr = abstr.replace("&lt;", "<")
                    
                absText.append(abstr)            
        text = " ".join(absText)
    return text

def get_deletionListFromXML(text):
    auxList = text.split(DELETION_SPLIT)
    if len(auxList) == 1:
        return []
    deletionList = auxList[1]
    deletionList = deletionList.replace("</DeleteCitation>","").replace("</MedlineCitationSet>", "").replace("\n", "").strip()
    pmidList = deletionList.replace("<PMID Version=\"1\">","").split(PMID_SPLIT)
    if not len(pmidList[-1]):
        del pmidList[-1]
    return pmidList

def get_pmids_records(doc_conn, pmid_list = []):
    if len(pmid_list):
        doc_records = doc_conn.find({"_id":{"$in":pmid_list}})
    else:
        doc_records = doc_conn.find()
    
    doc_conn.close()
    
    return doc_records
