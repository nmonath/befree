# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

from tqdm import tqdm
import sys, nltk
import MedlineUtils
current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from time import time

def extract_abstracts_by_line(input_file, new_coll_name, database_name, remove=True):
    med_conn = MongoConnection(database_name, new_coll_name)
    if remove:
        med_conn.delete_collection()
    xml_str = ""
    
    # aBravo Add 06.02.17
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    # aBravo 
    
    progvar = 0
    j=0
    for lin in tqdm(open(input_file)):
        progvar += 1
        # print(progvar)
        #progress.update(progvar)
        if not "<MedlineCitation " in lin:
            xml_str+=lin
            continue
        if j:
            newDoc = MedlineUtils.get_document(xml_str.strip(),tokenizer)
            med_conn.insert_document(newDoc)
        xml_str=lin
        j=1
    med_conn.close()


if __name__ == '__main__':
    
    start_time_total = time()
    input_path = sys.argv[1]
    new_coll_name = sys.argv[2]
    database_name = sys.argv[3]
    
    removing = True
    if len(sys.argv) > 4:
        removing_str = sys.argv[4].lower()
        if "0" in removing_str or "f" in removing_str or "n" in removing_str:
            removing = False
    extract_abstracts_by_line(input_path, new_coll_name, database_name, removing)
        
    print("TOTAL TIME", time() - start_time_total, "seconds")
