# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

TAG_SPLIT = "<MedlineCitation "
PMID_SPLIT = "</PMID>"
NO_MESH_HEADING_LIST = "<MeshHeadingList/>"
MESH_HEADING_LIST_INI = "<MeshHeadingList>"
MESH_HEADING_LIST_END = "</MeshHeadingList>"
MESH_HEADING_TAG = "MeshHeading"
DATE_CREATED_TAG = "DateCreated" 
DESCRIPTOR_TAG = "DescriptorName"
QUALIFIER_TAG = "QualifierName"
SPLIT_CHAR = "/"
DELETION_SPLIT = "<DeleteCitation>"
PUB_DATE_TAG = "PubDate"
YEAR_TAG = "Year"
MONTH_TAG = "Month"
DAY_TAG = "Day"

TITLE_INI_TAG = "<ArticleTitle>"
TITLE_END_TAG = "</ArticleTitle>"
TEXT_INI_TAG = "<AbstractText"
TEXT_END_TAG = "</AbstractText>"

ID_FIELD = "_id"
XML_FIELD = "xml_text"
MESH_FIELD = "mesh_list"
ABBREV_FIELD = "abbre"
MESH_DESC_FIELD = "mesh_desc_list"
MESH_QUAL_FIELD = "mesh_qual_list"
MESH_IDS_FIELD = "mesh_list_with_ids"

#DATE_FIELD = "date_created"
TITLE_FIELD = "title"
ABSTRACT_FIELD = "abstract_text"
LABEL_LIST_FIELD = "section_list"
ABSTRACT_LIST_FIELD = "section_text_list"
# aBravo Add 06.02.17
ABSTRACT_LIST_BY_SENT_FIELD = "section_text_by_sentences_list"
# aBravo
YEAR_FIELD = "year"
#JOURNAL_FIELD = "journal"
JOURNAL_TITLE_FIELD = "journal_title"
JOURNAL_ABBRE_FIELD = "journal_abbre"

ISSN_FIELD = "issn"
KEYWORDS_FIELD = "keywords"
