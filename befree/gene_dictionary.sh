#!/bin/bash

####################################################################
#
#	 Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.
#
#	 BeFree is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BeFree is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#	 How to cite BeFree:
#	 Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
#	 Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.
#
####################################################################

#######################
#     PARAMETERS      #
#######################
DIRPATH="/home/.../befree"

#HGNC files
HGNC_PATH="$DIRPATH/in/hgnc_complete_set.txt"
HGNC_HEADER="y"

#NCBI files
NCBI_PATH="$DIRPATH/in/Homo_sapiens.gene_info"
NCBI_HEADER="y"
NCBI_HIST_PATH="$DIRPATH/in/gene_history"
NCBI_HIST_HEADER="y"
#UNIPROT files
UNIPROT_PATH="$DIRPATH/in/uniprot_sprot_human.dat"
UNIPROT_HEADER="n"
UNIPROT_MAPPING_PATH="$DIRPATH/in/HUMAN_9606_idmapping_selected.tab"
UNIPROT_MAPPING_HEADER="n"

#SCRIPT files
HGNC_SCRIPT_PATH="$DIRPATH/src/dictionaries/hgnc_parser.py"
NCBI_SCRIPT_PATH="$DIRPATH/src/dictionaries/ncbi_parser.py"
UNIPROT_SCRIPT_PATH="$DIRPATH/src/dictionaries/uniprot_parser.py"
GENE_DICTIONARY_SCRIPT_PATH="$DIRPATH/src/dictionaries/gene_dictionary.py"

#######################
#     EXECUTION       #
#######################

PYTHON="python"

$PYTHON $HGNC_SCRIPT_PATH $HGNC_PATH $HGNC_HEADER
$PYTHON $NCBI_SCRIPT_PATH $NCBI_PATH $NCBI_HEADER $NCBI_HIST_PATH $NCBI_HIST_HEADER
$PYTHON $UNIPROT_SCRIPT_PATH $UNIPROT_PATH $UNIPROT_HEADER $UNIPROT_MAPPING_PATH $UNIPROT_MAPPING_HEADER
$PYTHON $GENE_DICTIONARY_SCRIPT_PATH
